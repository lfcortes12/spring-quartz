package com.globant.quartzswaflights.context;


public class MaintenanceOportunityContext {

    private String externalKey;
    private String departureDate;

    public String getExternalKey() {
        return externalKey;
    }

    public String getDepartureDate() {

        return departureDate;
    }

    public void setExternalKey(String externalKey) {
        this.externalKey = externalKey;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }
}
