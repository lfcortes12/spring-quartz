package com.globant.quartzswaflights;


import com.globant.quartzswaflights.context.MaintenanceOportunityContext;
import com.globant.quartzswaflights.job.MOPPJob;
import org.quartz.*;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.TaskScheduler;

public class Main {

    public static void main(String[] args) throws SchedulerException {

        MaintenanceOportunityContext maintenanceOportunityContext = new MaintenanceOportunityContext();
        maintenanceOportunityContext.setDepartureDate("2016-01-01 12:01:01");
        maintenanceOportunityContext.setExternalKey("SWA001DALJKL");

        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("quartz-beans.xml");

        JobBuilder jobBuilder = JobBuilder.newJob(MOPPJob.class);
        JobDataMap data = new JobDataMap();
        data.put("maintenanceOportunityContext", maintenanceOportunityContext);

        JobDetail jobDetail = jobBuilder.usingJobData("example", "com.globant.QuartzSchedulerExample")
                .usingJobData(data)
                .withIdentity("moppJob", "group1")
                .build();


        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("myTrigger", "group1")
                .startNow()
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withRepeatCount(3).withIntervalInSeconds(2))
                .build();

        Scheduler scheduler = (Scheduler) ctx.getBean("scheduler");
        scheduler.scheduleJob(jobDetail, trigger);

    }

}
