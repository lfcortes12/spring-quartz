package com.globant.quartzswaflights.job;

import com.globant.quartzswaflights.context.MaintenanceOportunityContext;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class MOPPJob extends QuartzJobBean {

    //TODO inject DB API for mopp delay
    //TODO inject dispatcher

    @Override
    public void executeInternal(JobExecutionContext context) throws JobExecutionException {
        System.out.println("Executing mopp job");
        JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
        MaintenanceOportunityContext moppContext = (MaintenanceOportunityContext) jobDataMap.get("maintenanceOportunityContext");

        //TODO search for STATUS column in MOPP_JOB_DETAILS1 table
        //TODO update MOPP_JOB_DETAILS1 table

        System.out.println("Completing mopp job: " + moppContext.getExternalKey());
    }
}
